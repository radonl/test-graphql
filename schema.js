const graphql = require('graphql');
const {GraphQLObjectType, GraphQLSchema} = graphql;

const categoryQueries = require('./queries/category.query');
const subCategoryQueries = require('./queries/subcategory.query');
const categoryMutation = require('./mutations/category.mutation');
const subCategoryMutation = require('./mutations/subcategory.mutation');

const Query = new GraphQLObjectType({
  name: `Query`,
  fields: {
    ...categoryQueries,
    ...subCategoryQueries
  }
});


const Mutation = new GraphQLObjectType({
  name: `Mutation`,
  fields: {
    ...categoryMutation,
    ...subCategoryMutation
  }
});


module.exports = new GraphQLSchema({
  query: Query,
  mutation: Mutation
});
