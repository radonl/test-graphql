require('dotenv').config();

const express = require('express');
const graphqlHttp = require('express-graphql');
const schema = require('./schema');
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const app = express();
app.use('/graphql', graphqlHttp({
  schema,
  graphiql: true
}));

app.listen(process.env.PORT, err => err ? console.log(err) : console.log('Server started!'));
