const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategoryModelSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
});

module.exports = mongoose.model('Category', CategoryModelSchema);
