const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SubCategoryModelSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  categoryId: {
    type: Schema.Types.ObjectId,
    ref: 'Category'
  }
});

module.exports = mongoose.model('SubCategory', SubCategoryModelSchema);
