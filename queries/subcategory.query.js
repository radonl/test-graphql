const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLString, GraphQLList} = graphql;

const SubCategoryType = require('../types/subcategory.type');
const SubCategory = require('../models/subcategory');

module.exports = {
  subCategory: {
    type: SubCategoryType,
    args: {
      _id: {
        type: GraphQLString
      }
    },
    resolve: (parent, {_id}) => {
      return SubCategory.findOne({
        _id: ObjectId(_id)
      });
    }
  },
  subCategories: {
    type: GraphQLList(SubCategoryType),
    resolve: (parent, args) => {
      return SubCategory.find({});
    }
  }
};
