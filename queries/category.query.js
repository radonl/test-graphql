const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLString, GraphQLList} = graphql;

const CategoryType = require('../types/category.type');
const Category = require('../models/category');

module.exports = {
  category: {
    type: CategoryType,
    args: {
      _id: {
        type: GraphQLString
      }
    },
    resolve: (parent, {_id}) => {
      return Category.findOne({
        _id: ObjectId(_id)
      });
    }
  },
  categories: {
    type: GraphQLList(CategoryType),
    resolve: (parent, args) => {
      return Category.find({});
    }
  },
};
