const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLObjectType, GraphQLString, GraphQLList} = graphql;

const SubCategory = require('../models/subcategory');

const CategoryType = new GraphQLObjectType({
  name: `Category`,
  description: `Category`,
  fields: () => ({
    _id: {
      type: GraphQLString
    },
    name: {
      type: GraphQLString
    },
    subCategories: {
      type: GraphQLList(SubCategoryType()),
      resolve: ({_id}, args) => {
        return SubCategory.find({
          categoryId: ObjectId(_id)
        });
      }
    }
  })
});

const SubCategoryType = () => require('./subcategory.type');

module.exports = CategoryType;
