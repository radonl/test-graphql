const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLObjectType, GraphQLString} = graphql;

const Category = require('../models/category');

const SubCategoryType = new GraphQLObjectType({
  name: `SubCategory`,
  description: `SubCategory`,
  fields: () => ({
    _id: {
      type: GraphQLString
    },
    name: {
      type: GraphQLString
    },
    category: {
      type: CategoryType(),
      resolve: ({categoryId}, args) => {
        return Category.findOne({
          _id: new ObjectId(categoryId)
        });
      }
    }
  })
});

const CategoryType = () => require('./category.type');

module.exports = SubCategoryType;
