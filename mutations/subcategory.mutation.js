const graphql = require('graphql');
const ObjectId = require('mongoose').Types.ObjectId;
const {GraphQLString, GraphQLNonNull} = graphql;

const SubCategoryType = require('../types/subcategory.type');
const SubCategory = require('../models/subcategory');

module.exports = {
  addSubCategory: {
    type: SubCategoryType,
    args: {
      name: {
        type: GraphQLNonNull(GraphQLString)
      },
      categoryId: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {name, categoryId}) => {
      const subCategory = new SubCategory({
        name,
        categoryId: new ObjectId(categoryId)
      });
      subCategory.save();
      return subCategory;
    }
  },
  removeSubCategory: {
    type: SubCategoryType,
    args: {
      id: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {id}) => {
      return SubCategory.findOneAndDelete(id);
    }
  },
  updateSubCategory: {
    type: SubCategoryType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      },
      name: {
        type: GraphQLNonNull(GraphQLString)
      },
      categoryId: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {_id, name, categoryId}) => {
      return SubCategory.findOneAndUpdate(_id, {
        $set: {name, categoryId}
      }, {
        new: true
      })
    }
  }
};
