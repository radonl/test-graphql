const graphql = require('graphql');
const {GraphQLString, GraphQLNonNull} = graphql;

const CategoryType = require('../types/category.type');
const Category = require('../models/category');

module.exports = {
  addCategory: {
    type: CategoryType,
    args: {
      name: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, args) => {
      const category = new Category(args);
      category.save();
      return category;
    }
  },
  removeCategory: {
    type: CategoryType,
    args: {
      id: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {id}) => {
      return Category.findOneAndDelete(id);
    }
  },
  updateCategory: {
    type: CategoryType,
    args: {
      _id: {
        type: GraphQLNonNull(GraphQLString)
      },
      name: {
        type: GraphQLNonNull(GraphQLString)
      }
    },
    resolve: (parent, {_id, name}) => {
      return Category.findOneAndUpdate(_id, {
        $set: {name}
      }, {
        new: true
      })
    }
  },
};
